import { Component, OnInit } from '@angular/core';
import { AdminService } from '../service/admin.service';
import { SecurityService } from '../service/security.service';
import { Product } from '../utils/product';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  constructor(private adminService:AdminService, private securityService:SecurityService) { }

  ngOnInit(): void {
    this.adminCall()
  }

  productId:number=0
  product = new Product()

  adminCall() {
    this.securityService.adminCall().subscribe((data) => {
      console.log(data)
    })
  }
  
  increaseCounter() {
    this.productId = this.productId + 1
  }
  
  addProduct() {
    this.adminService.addProduct(this.product).subscribe((data) => {
      console.log(data)
    })
  }

  updateProduct() {
    this.adminService.updateProduct(this.product).subscribe((data) => {
      console.log(data)
    })
  }

  removeProduct() {
    this.adminService.removeProduct(this.productId).subscribe
  }

}
