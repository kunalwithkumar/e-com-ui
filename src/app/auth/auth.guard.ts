import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../service/auth.service';
import { SecurityService } from '../service/security.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private authService: AuthService,
    private router: Router,
        private securityService:SecurityService
  ) { }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    
    if (this.authService.getToken() !== null) {
      const role = route.data["roles"] as Array<String>
      
      if (role) {
        const match = this.securityService.roleMatch(role)

        if (match) {
          return true
        } else {
          this.router.navigate(['/error'])
          return false
        }
      }
    }
    this.router.navigate(['/login'])
    return false
    
  }
  
}
