import { Component, OnInit } from '@angular/core';
import { UserService } from '../service/user.service';
import { Cart } from '../utils/cart';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  constructor(private userService:UserService) { }

  ngOnInit(): void {
  }

  userId:Number=101
  carts:Cart[] | undefined

  getCart() {
    this.userService.getCart(this.userId).subscribe((data) => {
      console.log(data)
      this.carts=data
    })
  }

  cart= new Cart()
  removeFromCart() {
    this.userService.removeFromCart(this.cart).subscribe((data) => {
      console.log(data)
    })
  }


}
