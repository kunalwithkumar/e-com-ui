import { Component, OnInit } from '@angular/core';
import { SecurityService } from '../service/security.service';
import { UserService } from '../service/user.service';
import { Product } from '../utils/product';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private userService: UserService,
  private securityService:SecurityService) { }

  ngOnInit(): void {
    this.getAllProducts()
    console.log (this.securityService.roleMatch(['USER']));
  }

  products: Product[]|undefined
  getAllProducts() {
    this.userService.getProduct().subscribe((data) => {
      console.log(data)
      this.products=data
    })
  }

}
