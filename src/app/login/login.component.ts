import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { AdminService } from '../service/admin.service';
import { AuthService } from '../service/auth.service';
import { SecurityService } from '../service/security.service';
import { UserService } from '../service/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private userService: UserService,
    private adminService: AdminService,
    private securityservice: SecurityService,
    private router: Router,
    private authService:AuthService
  ) { }

  loginDTO= {
    userName: String,
    password:String
  }

  ngOnInit(): void {
  }

  adminLogin() {
    this.adminService.login(this.loginDTO).subscribe((data) => {
      console.log(data)
    })
  }

  userLogin(loginForm: NgForm) {
    this.securityservice.login(loginForm.value).subscribe((data:any) => {
      this.authService.setRoles(data.user.authorities)
      this.authService.setToken(data.token)

      if (data.user.authorities[0].authority === 'USER') {
        this.router.navigate(['/user'])
      }
      else if (data.user.authorities[0].authority === 'ADMIN'){
        this.router.navigate(['/admin'])
      }
    }, (error) => {
      console.log(error)
      this.router.navigate(['/error'])
    }
    )
  }

  

}
