import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../service/auth.service';
import { SecurityService } from '../service/security.service';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {

  constructor(private authService: AuthService,
    private router: Router,
    private securityService:SecurityService
  ) { }

  ngOnInit(): void {
    //this.whichUser()
  }


  item:String='Hello'

  getValue(data:any) {
    
  }

  isLoggedIn() {
   return this.authService.isLoggedIn()
  } 
  
  logout() {
    this.authService.clear()
    this.router.navigate(['/home'])

  }
  role: any
  admin: boolean = true
  user:boolean=true
  whichUser() {
    this.role = this.securityService.getRoles();
    if (JSON.parse(this.role)[0].authority === "USER")
      this.admin = false
    this.user=false
  }

}
