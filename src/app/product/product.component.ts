import { Component, OnInit } from '@angular/core';
import { UserService } from '../service/user.service';
import { Product } from '../utils/product';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.getProducts()
  }
  product :Product[] | undefined

  getProducts(){
    this.userService.getProduct().subscribe((data) => {
      console.log(data)
      this.product=data
    })
  }
  
}
