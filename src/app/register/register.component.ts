import { Component, OnInit } from '@angular/core';
import { AdminService } from '../service/admin.service';
import { UserService } from '../service/user.service';
import { Admin } from '../utils/admin';
import { User } from '../utils/user';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(private userService:UserService, private adminService:AdminService) { }

  ngOnInit(): void {
  }

  admin = new Admin()
  user= new User()

  adminRegister() {
    this.adminService.register(this.admin).subscribe((data) => {
      console.log(data)
    })
  }

  userRegister() {
    this.userService.register(this.user).subscribe((data) => {
      console.log(data)
    })
  }

}
