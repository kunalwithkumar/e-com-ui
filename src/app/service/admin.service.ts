import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Admin } from '../utils/admin';
import { Product } from '../utils/product';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  constructor(private http: HttpClient) { }
  
  url: String = 'http://localhost:9090/admin/'
  
  register(admin: Admin): Observable<Admin>{
    return this.http.post<Admin>(this.url + 'register', admin)
  }

  login(loginDTO: any): Observable<Admin>{
    return this.http.post<Admin>(this.url+'login',loginDTO)
  }

  logout(adminId: Number): Observable<Admin>{
    return this.http.get<Admin>(this.url+'logout/'+adminId)
  }

  addProduct(product: Product): Observable<Product[]>{
    return this.http.post<Product[]>(this.url+'addProduct',product)
  }

  updateProduct(product: Product): Observable<Product[]>{
    return this.http.put<Product[]>(this.url+'updateProduct',product)
  }

  removeProduct(productId: Number): Observable<String>{
    return this.http.delete<String>(this.url+'removeProduct/'+productId)
  }

  removeUser(userId: Number): Observable<String>{
    return this.http.delete<String>(this.url+'removeUser/'+userId)
  }
  
}
