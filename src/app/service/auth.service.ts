import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  setRoles(role: []) {
    localStorage.setItem('roles',JSON.stringify(role))
  }

  role:any
  getRoles(): []{
    this.role = (localStorage.getItem('roles'))
    return this.role
  }


  setToken(jwtToken: string) {
    localStorage.setItem('jwtToken',jwtToken)
  }

  token:any
  getToken(): string{
   this. token =
     localStorage.getItem('jwtToken')
    return this.token
  }

  clear() {
    localStorage.clear()
  }
  isLoggedIn() {
    return this.getRoles() && this.getToken()
  }
}
