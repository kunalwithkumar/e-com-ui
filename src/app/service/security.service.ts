import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class SecurityService {

  constructor(private http: HttpClient,
  private authService:AuthService) { }
  
  url: String = 'http://localhost:9595/api/v1/auth/'

  requestHeader = new HttpHeaders(
    {"No-Auth":"True"}
  )
  
  login(loginForm: any) {
    return this.http.post(this.url+'authenticate',loginForm,{headers:this.requestHeader})
  }

  userCall():Observable<Object> {
    return this.http.get('http://localhost:9595/api/v1/security/user/user@mail.com')

  }

  adminCall() {
    return this.http.get('http://localhost:9595/api/v1/security/all')

  }

  role:any
  getRoles(): []{
    this.role = (localStorage.getItem('roles'))
    return this.role
  }

  roleMatch(allowedRoles: any): boolean {
    let isMatch = false;
    this.role = this.authService.getRoles()
    this.getRoles()
    console.log(this.role)
          if ((JSON.parse(this.role)[0].authority) === allowedRoles[0]) {
            isMatch = true;
            return isMatch;
          } else {
            return isMatch;
          }
        
  }


}
