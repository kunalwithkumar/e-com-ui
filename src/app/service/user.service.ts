import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Cart } from '../utils/cart';
import { Product } from '../utils/product';
import { User } from '../utils/user';
import { Wishlist } from '../utils/wishlist';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http :HttpClient) { }

  url: String = 'http://localhost:9090/user/'
  
  user= new User()

  getUserData(user:User) {
     this.user=user
  }

  giveUserData() {
    return this.user
  }

  getProduct():Observable<Product[]>{
    return this.http.get<Product[]>(this.url+'getProducts')
  }
  
  register(user:User):Observable<User> {
    return this.http.post<User>(this.url+'register',user)
  }

  login(loginDTO:any){
    return this.http.get<User>(this.url + 'login', loginDTO)
  }

  logout(userId: Number):Observable<String> {
    return this.http.get<String>(this.url+'logout/'+userId)
  }
  
  update(user: User): Observable<User>{
    return this.http.put<User>(this.url+'update',user)
  }

  addToCart(cart: Cart) :Observable<Cart[]>{
    return this.http.post<Cart[]>(this.url+'addToCart',cart)
  }

  addToWishList(wishList: Wishlist): Observable<Wishlist[]>{
    return this.http.post<Wishlist[]>(this.url+'addToWishList',wishList)
  }

  getCart(userId: Number): Observable<Cart[]>{
    return this.http.get<Cart[]>(this.url+'getCart/'+userId)
  }

  getWishList(userId: Number): Observable<Wishlist[]>{
    return this.http.get<Wishlist[]>(this.url + 'getWishList/' + userId)
  }  

  removeFromCart(cart: any){
    return this.http.delete<String>(this.url + 'removeFromCart', cart)
  }

  removeFromWishList(wishList: any){
    return this.http.delete<String>(this.url+'removeFromWishList',wishList)
  }

}

