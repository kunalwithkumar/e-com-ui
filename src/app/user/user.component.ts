import { Component, OnInit } from '@angular/core';
import { SecurityService } from '../service/security.service';
import { UserService } from '../service/user.service';


@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  constructor(private userService: UserService, private securityService:SecurityService) { }

  ngOnInit(): void {
    this.getProducts()
    this.userCall()
    
  }

  getProducts(){
    this.userService.getProduct().subscribe((data) => {
      console.log(data)
    })
  }

  userCall() {
    this.securityService.userCall().subscribe((data) => {
      console.log(data+"data from user")
    })
  }

}
