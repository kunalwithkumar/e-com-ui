import { Product } from "./product"
import { User } from "./user"

export class Cart {
    
     id:Number=0
	
	
	  user:User | undefined
	
	  product:Product | undefined
	
	  quantity:Number=0
}
