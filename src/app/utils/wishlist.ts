import { Product } from "./product"
import { User } from "./user"

export class Wishlist {

    id: Number = 0
    user: User | undefined
    product:Product |undefined

}
