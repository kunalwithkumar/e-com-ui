import { Component, OnInit } from '@angular/core';
import { UserService } from '../service/user.service';
import { Wishlist } from '../utils/wishlist';


@Component({
  selector: 'app-wish-list',
  templateUrl: './wish-list.component.html',
  styleUrls: ['./wish-list.component.css']
})
export class WishListComponent implements OnInit {

  constructor(private userService: UserService) { }
  
  ngOnInit(): void {
    this.getWishList()
  }

  userId: Number = 101

  wishLists:Wishlist[] | undefined
  wishList= new Wishlist()


  getWishList() {
    this.userService.getWishList(this.userId).subscribe((data) => {
      console.log(data)
      this.wishLists=data
    })
  }
  
  removeFromWishList() {
    this.userService.removeFromWishList(this.wishList).subscribe((data) => {
      console.log(data)
    })
  }

}